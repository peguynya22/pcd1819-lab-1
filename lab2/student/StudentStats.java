package lab2.student;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StudentStats {
	
	public static final void main(String args[]) {

		List<Student> students = IntStream.range(1, 50)
										  .mapToObj(it->
										  			new Student(StudentUtil::generateName,
													StudentUtil::generateGrade,
													StudentUtil::generateDepartment,
													StudentUtil::generateGender))
										  .collect(Collectors.toList());
		
		//1. compute the number of students with passing grades.
		int admissible = students.stream().filter(Student->Student.getGrade() >= 18).count();
		//2. compute the  average student grade.
		//maptoint returns and IntStream and we can apply Average()method.
		 double av = students.stream().mapToInt(x-> x=Student.getGrade()).average();
		 //alternative solution usando averaging della metod collect.
		 double avalt = students.stream().collect(collectors.averagingInt(Student->Student.getGrade()));
		//3. partition the students to their belonging department.
		 List<Student> studentByFaculty = students.stream().collect(collectors.groupingBy(Student->Student.getDepartment()))
		//4. compute the average grade by department.
		 double averageByDepartment = students.stream()
		 .collect(collectors.groupingBy(Student::getDepartment())
		 .averagingInt(Student->Student.getGrade()));
}
